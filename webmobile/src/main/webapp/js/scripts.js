var timerSensorLuz;

$(document).ready(function() {
	
	$(".js-nav-bar").click(function() {
		cleanAlert();
		if ($(this).attr("name") == "led") {
			removeIntervalSensorLuz();
			$(".div-sensor-luminosidade").hide();
			$(".div-led").show();
		} else if ($(this).attr("name") == "sensor-luminosidade") {
			addIntervalSensorLuz();
			$(".div-sensor-luminosidade").show();
			$(".div-led").hide();
		}
	});

	$(".js-select-led").change(function() {
		var name = $(this).attr("led");
		var value = $(this).val();
		var led = name + value;
		cleanAlert();
		callSerialWriteLed(led);
	});
});

function callSerialWriteLed(led) {
	$.ajax({
        type: "POST",
        url: "utils/led.jsp",
        data: {'led':led},
        async: false
    }).done(function() {
		$("alert-success").show();
	}).fail(function() {
		$(".alert-warning").show();
	});
}

function callSearchValSensorLuz() {
	$.ajax({
        type: "POST",
        url: "utils/sensor_luz.jsp",
        async: false
    }).done(function(data) {
    	$(".js-sensor-luminosidade").text(data);
	}).fail(function() {
		$(".alert-warning").show();
	});
}

function removeIntervalSensorLuz() {
	clearInterval(timerSensorLuz);
}

function addIntervalSensorLuz() {
	timerSensorLuz = setInterval(callSearchValSensorLuz, 500);
}

function cleanAlert() {
	$(".alert-success").hide();
	$(".alert-warning").hide();
}