<html>
<head>
	<meta charset="utf-8">
	
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" href="css/style.css">
	
	<link rel="stylesheet" href="css/jquery/jquery.mobile-1.4.5.min.css">
	
	<script type="text/javascript" src="js/jquery/jquery-1.11.1.min.js"></script>
		
	<script type="text/javascript" src="js/jquery/jquery.mobile-1.4.5.min.js"></script>
	
	<script type="text/javascript" src="js/scripts.js"></script>
</head>

<body>
	<div data-role="navbar">
		<ul>
			<li>
				<a name="led" href="#" class="ui-btn-active ui-btn-b js-nav-bar">Luzes</a>
			</li>
			<li>
				<a name="sensor-luminosidade" href="#" class="ui-btn-b js-nav-bar">Sensor</a>
			</li>
		</ul>
	</div>
	<!-- /navbar -->

	<div class="div-led">
		<form>
			<label for="led-1">Led Azul:</label>
			<select name="led-1" led="1" data-role="flipswitch" data-theme="b" class="js-select-led">
				<option value="0">Off</option>
				<option value="1">On</option>
			</select>
			
			<label for="led-2">Led Vermelho:</label>
			<select name="led-2" led="2" data-role="flipswitch" data-theme="b" class="js-select-led">
				<option value="0">Off</option>
				<option value="1">On</option>
			</select>
			
			<label for="led-3">Led Amarelo:</label>
			<select name="led-3" led="3" data-role="flipswitch" data-theme="b" class="js-select-led">
				<option value="0">Off</option>
				<option value="1">On</option>
			</select>
		</form>
	</div>

	<div class="div-sensor-luminosidade" style="display: none;">
		<div style="height: 319px;">
			<p>
				Data <br>17/06/2016
			</p>
			<br>Sensor de luminosidade <span class="js-sensor-luminosidade" style="display: block"></span>
		</div>
	</div>
	
	<div class="alert alert-success" style="display: none;">
		<strong>Sucesso!</strong>Sucesso na a��o realizada!
	</div>
	
	<div class="alert alert-warning" style="display: none;">
		<strong>Aten��o!</strong> Pode ter ocorrido algum problema!
	</div>
</body>
</html>