
<%@page import="service.SerialCommunicationSingleton"%>
<%
	SerialCommunicationSingleton serial;
	if (session.getAttribute("serial") == null) {
		serial = SerialCommunicationSingleton.getInstance();
		session.setAttribute("serial", serial);
	} else {
		serial = (SerialCommunicationSingleton)session.getAttribute("serial");
	}
 	serial.doWriteLed(request.getParameter("led"));
%>