
int value = 0;
int serialA = A1;
int valor = 0;
int led8 = 8;
int led12 = 12;
int led13 = 13;
int ldrPin = 0; //LDR no pino analígico 8
int ldrValor = 0; //Valor lido do LDR
 

void setup() {
  pinMode(led8, OUTPUT);
  pinMode(led12, OUTPUT);
  pinMode(led13, OUTPUT);
  
  Serial.begin(9600);
}

void loop() {

  if (Serial.available()) {
    value = Serial.parseInt();
  }
  switch(value) {
    // Led Azul
    case 10:
      digitalWrite(led13, LOW);
      break;
    case 11:
      digitalWrite(led13, HIGH);
      break;
    
    // Led Vermelho
    case 20:
      digitalWrite(led12, LOW);
      break;
    case 21:
      digitalWrite(led12, HIGH);
      break;
    
    // Led Amarelo
    case 30:
      digitalWrite(led8, LOW);
      break;
    case 31:
      digitalWrite(led8, HIGH);
      break;
  }
 delay(500);
 ///ler o valor do LDR
 ldrValor = analogRead(ldrPin); //O valor lido será entre 0 e 1023
 delay(500); 
 //imprime o valor lido do LDR no monitor serial
 Serial.println(ldrValor);
}
