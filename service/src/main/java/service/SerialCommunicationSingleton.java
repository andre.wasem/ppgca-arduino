package service;

import java.io.IOException;
import java.util.Scanner;

import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortDataListener;
import com.fazecast.jSerialComm.SerialPortEvent;

public class SerialCommunicationSingleton {
	
	private String valor;
	
	private static SerialCommunicationSingleton singleton;
	
	private static final SerialPort serialPort = SerialPort.getCommPorts()[0];
	
	private SerialCommunicationSingleton() {
		openPorta();
		serialPort.setComPortTimeouts(SerialPort.TIMEOUT_WRITE_BLOCKING, 0, 0);
	}
	
	public static synchronized SerialCommunicationSingleton getInstance() {
		if (singleton == null) {
			singleton = new SerialCommunicationSingleton();
		}
		return singleton;
	}
	
	private final void openPorta() {
		if (!serialPort.isOpen()) {
			if (serialPort.openPort()) {
				System.out.println("Sucesso!");
			} else {
				System.out.println("Erro!");
			}
		}
	}
	
	public void doWriteLed(String led) {
		try {
			serialPort.getOutputStream().write(led.getBytes());
			serialPort.getOutputStream().flush();
			serialPort.getOutputStream().close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		serialPort.bytesAvailable();
	}
	
	public void addEventListener() {
		serialPort.addDataListener(new SerialPortDataListener() {
			@Override
			public void serialEvent(SerialPortEvent event) {
				if (event.getEventType() != SerialPort.LISTENING_EVENT_DATA_RECEIVED) {
					return;
				}
				
				Scanner data = new Scanner(serialPort.getInputStream());
				if (data.hasNext()) {
					String valor = data.next();
					if (valor != null && !valor.equals(getValor())) {
						setValor(valor);
					}
				}
			}
			
			@Override
			public int getListeningEvents() {
				return serialPort.LISTENING_EVENT_DATA_RECEIVED;
			}
		});
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}
}
